@ECHO OFF
TITLE Firefox Profile Backup
SETLOCAL
IF EXIST "%programfiles%\7-Zip\7Z.exe" goto cont
echo Install 7zip from 7zip.org
exit

:cont
FOR /F "skip=1 tokens=1-6" %%G IN ('WMIC Path Win32_LocalTime Get Day^,Hour^,Minute^,Month^,Second^,Year /Format:table') DO (
   IF "%%~L"=="" goto ts_done
      Set _yyyy=%%L
      Set _mm=00%%J
      Set _dd=00%%G
      Set _hour=00%%H
      SET _minute=00%%I
      SET _second=00%%K
)
:ts_done
      Set _mm=%_mm:~-2%
      Set _dd=%_dd:~-2%
      Set _hour=%_hour:~-2%
      Set _minute=%_minute:~-2%
      Set _second=%_second:~-2%

Set logtimestamp=%_yyyy%-%_mm%-%_dd%_%_hour%_%_minute%_%_second%
SET BackupFileName=%USERPROFILE%\Documents\BackupFirefoxProfile_%logtimestamp%.zip
SET TempBackupDir=%TEMP%\Mozilla_Firefox_Profile
SET TempBackupDirAction="%TempBackupDir%"
IF EXIST %TempBackupDirAction% RMDIR %TempBackupDirAction%
MKDIR %TempBackupDirAction%
XCOPY "%APPDATA%\Mozilla\Firefox\Profiles\*" %TempBackupDirAction% /E /V /C /H /Y
SET BackupFileName="%BackupFileName%"
IF EXIST %BackupFileName% DEL /F /Q %BackupFileName%

"%programfiles%\7-Zip\7Z.exe" a %BackupFileName% "%TempBackupDir%*"
IF EXIST %TempBackupDirAction% RMDIR /S /Q %TempBackupDirAction%
ENDLOCAL