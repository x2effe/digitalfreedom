![Mass Surveilance](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/cover.jpg)
# Digital Freedom project: stop mass surveillance[^0]
__This is mainly an anti-Faceb00k[^8], Amazon, Google, Microsoft and Apple (aka FAGMA) repository, all-you-need resources needed to stop mass surveillance, user (meta) data collecting, ADs and telemetry.__
Why all this? Because nowadays...

> "The only way to maintain privacy on the internet is to not be on the internet."

Unfortunately it's true but we can't give up, we should fight for our rights.
The goal of this (little) project is to offer users a claner internet experience _for free._

And do not forget: for your safety, always choose decentralized and open source projects.

_For more info about privacy, telemetry and such, it is imperative to read carefully ![Privacytools webstite](http://privacytools.io)[^1]_

ToC
---
<pre>
° <a href="#firefox-the-only-and-the-one"
>Browser: Firefox  ......................................................</a> Firefox is really the best?
° <a href="#browser-alternatives"
>Browser: alternatives ..................................................</a> Browser alternatives - if any...
° <a href="#search-vs-privacy"
>Search engines .........................................................</a> Search vs privacy
° <a href="#dns-dot"
>Replace your ISPs DNS ..................................................</a> DNS over TLS is the future!
° <a href="#do-not-use-default-services"
>Opensource FAGMA ? .....................................................</a> Be smart: do not use X, use Y instead
</pre><hr>

![Firefox](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/firefox.jpg)
## Firefox - The only and the one

Unluckly, Mozilla is not different from the others and leaving the browser as it is, not a smart idea.
To apply my privacy settings, first make sure firefox is closed, then:
* copy all the files inside firefox\install directory contents of this repo to where you've installed firefox (usually %ProgramFiles%\Mozilla Firefox).
* copy all the files inside firefox\profile directory contents of this repo to your firefox profiles directory[^3]

In this directory, I've also added:

1) A [search engine compilation](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/firefox/profile/search.json.mozlz4), 
(more info on search engine below) just copy that file to your _mozilla firefox profile directory[^3]_ which contains:

- Albumart exchange (for your music cover needs)
- Intopic (metanews search engine for italy)
- Qobuz (the famous music streaming service)
- SearxNG

2) A [windows batch script for backing up mozilla profile folder](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/firefox/utils/make_fpb.bat), requires [7zip](https://www.7-zip.org).<br>
__Sidenote:__ for backing up profiles, I suggest [Simple firefox backup by sordum.org](https://www.sordum.org/12298/simple-firefox-backup-v1-2/).

![Br_alt](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/lf.jpg)
## Browser alternatives

![Betterfox](https://github.com/yokoffing/BetterFox), a pre-configured custom preference file for Mozilla Firefox, is a very good option, it is updated frequently, adding more tweaks and remove those obsolete.<br>
If you've time and/or you're brave enought, you can modify Firefox to enhance privacy by yourself, have a read [here for more info](https://restoreprivacy.com/firefox-privacy).<br>
On the other side, if you want a brower alternative, ![Librewolf](https://librewolf.net) (ex librefox) is a valid option too if you want strict privacy browser with everything pre-configured (not my choice since it blocks too much and some websites don't work correctly).

![search](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/search.jpg)<br>
## Search vs privacy
Even search engines don't play nice nowadays. After all, the key of those corporations is to make profit and the easy way is to do it is to collect your data and resell to 3rd parties. So, the only way is to choose the right one for you. For this, I've added a [search engine compilation](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/firefox/profile/search.json.mozlz4) for Mozilla Firefox and I'm currently modifying it, removing or adding new ones to the list.<br><br>
Some advices: for images, torrents and "such kind of stuff", use yandex[^5] since others censors certain websites.<br>
For ordinary search, I suggest to use qwant, if you're in EU. Alternatives are SearXNG (recomended) or Startpage (which is a bit slow here).

![DNS_DOT](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/dns.jpg)<br>
## DNS (DoT)
It is imperative to not use your ISPs DNSs since they log you and they do not provide any kind of security. For optimal performances, I strongly suggest you to use DNS over TLS (aka DoT).

__Sidenote: those pubished here are for european users, on their respective homepages you can find the server near you.__

IP|Hostname|Port|Website
----------- | --------------------------------- | --- | ------------------ |
176.9.93.198|dnsforge.de|853|https://dnsforge.de

All servers posted here, are providing internal ad-blocking, they do not log IPs and they are open source projects too.<br>
Of course there are a lot of free DNS service out there, however make sure to use those who does not log your IP.<br>
An additional personal tip, i would use some local private DNS server caching/blocking like SmartDNS (openwrt package) or adguard home.<br>
I would __totally avoid__ Cloudflare, any dns server in US/Canada and of course Google.

![alt_svc](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/fagma.jpg)
## Do not use 'default' services.
Say no to google AND ALL HIS FRIENDS. Most of the (free) services never play nice to us, the users, but thanks to some volunteers, we have valid alternatives. For example, [farside](https://farside.link) is a free service which includes several FOSS (free and open source) links to alternatives.
> THEY NEVER PLAY NICE. WE MUST ACT WISER!<br>
DO NOT USE GOOGLESEARCH. USE SEARXNG/Duckduckgo INSTEAD.<br>
DO NOT USE FACEBOOK. USE MASTODON INSTEAD.<br>
DO NOT USE TIKTOK. USE PROXYTOK INSTEAD.<br>
DO NOT USE YOUTUBE. USE PIPED OR INVIDIOUS INSTEAD.<br>
DO NOT USE REDDIT. USE TEDDIT INSTEAD.<br>

QUICK TIP: [LIbredirect](https://libredirect.github.io/), a firefox extension that redirects YouTube, Twitter, Instagram, and other requests to alternative privacy friendly frontends and backends, is a good start point.

![No to G](https://codeberg.org/x2effe/digitalfreedom/raw/branch/main/img/no2g.jpg)<br>

_Sincerely yours,_<br>
__DoppiaeFFe aka x2eFFe__
- [x] 99.9% lazy and 0.1% busy person
- [x] I'll update this page randomly (when I'm in the mood)
<br><br><br><br><br>

### Notes
[^0]: Ex-github repository, named 'adblock' (Unsorted material / personal backup)
[^1]: Moved to [Privacyguides](https://privacyguides.org), I still continue to use 'old' site
[^2]: Enter: <code>"ipset restore -! < $filename$ && iptables -I INPUT -m set --match-set ipf src -j DROP"</code>
to apply (an ipset ruleset named ipf will be created) - of course, replace $filename$ with the blocklist you previously downloaded and copied.
[^3]: %appdata%\Roaming\Mozilla\Firefox\Profiles\\[variablenumbersandlettersdirectory]
[^5]: Voluntarily not added to my search engine compilation
[^7]: Variable (long) string generated by selecting custom blocklist, see https://dnswarden.com/customfilter.html, DNS-Over-TLS section.
[^8]: Faceb00k, Meta and Instagram are basically the same sh*t